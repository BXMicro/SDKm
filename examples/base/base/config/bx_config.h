/**
  ******************************************************************************
  * @file   :   .h
  * @version:
  * @author :
  * @brief  :
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright(c) . BLUEX Microelectronics.
  * All rights reserved.</center></h2>
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BX_CONFIG_H__
#define __BX_CONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

/* exported define -----------------------------------------------------------*/

#define SWDCLK                                          ( UTILITY_IO_EN | NORMAL_MODE_IE )
#define SWDIO                                           ( UTILITY_IO_EN | NORMAL_MODE_IE )


#define QSPI_CS                                         ( SLEEP_RET_OUT_H | SLEEP_RET_OUT_EN | UTILITY_IO_EN )
#define QSPI_CLK                                        ( SLEEP_RET_OUT_EN | UTILITY_IO_EN )
#define QSPI_D0                                         ( SLEEP_RET_OUT_EN | UTILITY_IO_EN | NORMAL_MODE_IE )
#define QSPI_D1                                         ( SLEEP_RET_OUT_EN | UTILITY_IO_EN | NORMAL_MODE_IE )
#define QSPI_D2                                         ( SLEEP_RET_OUT_EN | UTILITY_IO_EN | NORMAL_MODE_IE )
#define QSPI_D3                                         ( UTILITY_IO_EN | NORMAL_MODE_IE )

#define GENERAL_PURPOSE_IO                              ( NORMAL_MODE_IE )
#define EXTERNAL_INTR_IO                                ( UTILITY_IO_EN | SLEEP_MODE_IE | NORMAL_MODE_IE )

/* exported types ------------------------------------------------------------*/
enum gpio_pad_vdd_type
{
    VDD_1V8 = 0,
    VDD_3V
};
enum io_config_mask
{
    NORMAL_MODE_IE = 0x1,
    SLEEP_MODE_IE = 0x2,
    UTILITY_IO_EN = 0x4,
    SLEEP_RET_OUT_EN = 0x8,
    SLEEP_RET_OUT_H = 0x10,
};
/* exported variables --------------------------------------------------------*/

/* exported constants --------------------------------------------------------*/

/* exported macros -----------------------------------------------------------*/

/* exported functions --------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __BX_CONFIG_H__ */

/******************** (C) COPYRIGHT BLUEX **********************END OF FILE****/
